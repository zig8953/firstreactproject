import React from 'react';
import { Link } from 'react-router-dom';

export const Menu = () => {
    return (
            <ul>
                <li>
                    <Link to="/">Home</Link>
                </li>
                <li>
                    <Link to="aboutUs">About</Link>
                </li>
                <li>
                    <Link to="information">Contacts</Link>
                </li>
            </ul>
    )
}