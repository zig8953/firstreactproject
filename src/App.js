import './App.css';
import { Switch, Route } from 'react-router-dom';
import { Information } from "./components/information";
import { AboutUs } from "./components/aboutUs";
import { Menu } from './components/Menu';

function App() {
  return (
      <div className="App">
          <header className="App-header">
              <div id="menu">
                  <Menu />
              </div>
              <div id="content">
              <Switch>
                  <Route exact path='/aboutUs' component={AboutUs } />
                  <Route exact path='/information' component={Information} />
              </Switch>
              </div>
          </header>
      </div> 
  );
}

export default App;
